# Uncomment lines below if you have problems with $PATH
SHELL := /usr/local/bin/zsh
#PATH := /usr/local/bin:$(PATH)

pngs = $(wildcard pngs/*.png)
bmps = $(wildcard bmps/*.bmp)

all:
	platformio -c vim run

format:
	clang-format -style=file -i src/*

upload:
	platformio -c vim run --target upload

clean:
	platformio -c vim run --target clean

program:
	platformio -c vim run --target program

uploadfs:
	platformio -c vim run --target uploadfs

update:
	platformio -c vim update

monitor:
	platformio device monitor

png2h: $(pngs:.png=.h)
	mv -f pngs/*.h include/

%.h: %.png
	convert $< -strip -background black -alpha remove -alpha off -resize 16x16 $<.png
	@ffmpeg -vcodec png -i $<.png -vcodec rawvideo -f rawvideo -pix_fmt rgb565 $<.raw
	xxd -c 16 -i $<.raw > $@
	@gsed -i 's/^unsigned char/const unsigned char/g' $@
