import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

function Header() {
  return (
    <Navbar collapseOnSelect expand="sm" bg="light">
      <Navbar.Brand href="#home">Message Panel</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Link href="https://gitlab.com/bettse/messagePanel/">
          Sourcecode (GitLab)
        </Nav.Link>
      </Nav>
    </Navbar>
  )
}

export default Header;
