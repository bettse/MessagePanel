import React, {useState, useEffect} from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ListGroup from 'react-bootstrap/ListGroup';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

import Header from './Header';
import ColorExample from'./colors.png';

function App() {
	const [messages, setMessages] = useState([]);

  useEffect(() => {
    async function getMessages() {
      try {
        const response = await fetch('/.netlify/functions/message');
        if(response.ok) {
          const json = await response.json();
          setMessages(json);
        }
      } catch(e) {
        console.log(e);
      }
    }
    getMessages();
  }, []);

  const handleSubmit = async event => {
    event.preventDefault();
    const { target } = event;
    const formData = new FormData(target);
    const object = Object.fromEntries(formData);

    try {
      const response = await fetch('/.netlify/functions/message', {
        method: "POST",
        body: JSON.stringify(object),
      });
      if(response.ok) {
        const message = await response.json();
        setMessages([message, ...messages]);
      }
    } catch(e) {
      console.log(e);
    }
  }

  return (
    <div className="App">
      <Header />
      <Container className="pt-5" fluid='md'>
        <Row>
          <Col md={12} lg={6}>
            <Card>
              <Card.Body>
                <Card.Img variant="top" src={ColorExample} />
                <Card.Text>• {`Use <x> to set size.  1: half height 2: full height.`}</Card.Text>
                <Card.Text>• {`Use {x} to set color.`}</Card.Text>
                <Card.Text>• Can only fit 2.5 large(size 2) characters, or 10 small(size 1).</Card.Text>
                <Card.Text>• Messages used to scroll, but had to remove because of distraction.</Card.Text>
                <pre>{`Example: <2>{2}BIG RED`}</pre>
                <pre>{`Example: <1>{5}small green`}</pre>

                <Form onSubmit={handleSubmit}>
                  <Form.Group className="mb-3">
                    <Form.Label>Set Message</Form.Label>
                    <Form.Control type="text" name="value" placeholder="Enter a message" />
                  </Form.Group>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Form>
              </Card.Body>
            </Card>
					</Col>
          <Col className="text-center pt-5" md={12} lg={6}>
            <ListGroup>
              {messages.slice(0, 10).map((message, index) => {
                const { value } = message;
                return <ListGroup.Item key={index}>{value}</ListGroup.Item>;
              })}
            </ListGroup>
          </Col>
        </Row>
      </Container>
      <footer className="page-footer font-small pt-5 mt-5 mx-auto">
        <Container fluid className="text-center">
          <Row>
          </Row>
        </Container>
      </footer>
    </div>
  );
}

export default App;
