const fetch = require("node-fetch");
const cookie = require('cookie')
const { verify } = require('jsonwebtoken');

const {
  NETLIFY_DEV = false,
  IO_USERNAME,
  IO_KEY,
  IO_FEED,
  JWT_SECRET,
} = process.env;

const failure = {
  statusCode: 401,
  body: JSON.stringify({
    status: "There was an error processing your request",
    statusVariant: "danger",
  })
};

async function checkAuth(headers) {
  if (!headers.cookie) {
    return false;
  }

  const cookies = cookie.parse(headers.cookie);
  if (!cookies || !cookies.nf_jwt) {
    return false
  }
  const { nf_jwt } = cookies;
  try {
    const decoded = await verify(nf_jwt, JWT_SECRET);
    const { app_metadata } = decoded;
    const { authorization } = app_metadata;
    const { roles } = authorization
    return roles.includes('user');
  } catch (e) {
    return false;
  }
}

async function setMessage(value) {
  try {
    const url = `https://io.adafruit.com/api/v2/${IO_USERNAME}/feeds/${IO_FEED}/data`
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-AIO-Key": IO_KEY,
      },
      body: JSON.stringify({ value }),
    });
    if (response.ok) {
      return await response.json();
    }
    return {}
  } catch (e) {
    console.log("setMessage error", e);
  }
}

async function getMessages() {
  try {
    const url = `https://io.adafruit.com/api/v2/${IO_USERNAME}/feeds/${IO_FEED}/data`
    const response = await fetch(url, {
      headers: {
        "X-AIO-Key": IO_KEY,
      },
    });
    if (response.ok) {
      const json = await response.json();
      return json
    } else {
      return "[]";
    }
  } catch (e) {
    console.log("setMessage error", e);
  }
}

exports.handler = async function(event, context) {
  const { headers, httpMethod } = event;
  try {
    /*
    // make public
    const isUser = await checkAuth(headers);
    if (!isUser) {
      return failure;
    }

    */
    if (httpMethod === 'GET') {
      const messages = await getMessages();
      return {
        statusCode: 200,
        body: JSON.stringify(messages),
      };
    } else if (httpMethod === 'POST') {
      const {body: json} = event;
      const body = JSON.parse(json);
      const {value} = body;
      if (!value) {
        throw new Error('missing value');
      }
      const message = await setMessage(value);
      return {
        statusCode: 200,
        body: JSON.stringify(message),
      };
    }
  } catch (e) {
    console.error(e);
    return {
      statusCode: 422,
      body: JSON.stringify({
        status: e,
        statusVariant: "danger"
      })
    };
  }
  return failure;
};
