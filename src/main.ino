#define USE_AIRLIFT

#include "config.h"

#include <AdafruitIO.h>
#include <AdafruitIO_WiFi.h>
#include <AdafruitIO_Time.h>
#include <Adafruit_LIS3DH.h> // For accelerometer
#include <Adafruit_Protomatter.h> // For RGB matrix
#include <WiFiNINA.h>
#include <Wire.h> // For I2C communication
#include <utf8_decode.h>
#include <bmps.h>
#include <Bounce2.h>

#include <TimeLib.h>
#include <TimeAlarms.h>

// Adjust this number for the sensitivity of the 'click' force
// this strongly depend on the range! for 16G, try 5-10
// for 8G, try 10-20. for 4G try 20-40. for 2G try 40-80
#define CLICKTHRESHHOLD 80

// RGB MATRIX (PROTOMATTER) LIBRARY STUFF ----------------------------------

#define HEIGHT 16 // Matrix height (pixels) - SET TO 64 FOR 64x64 MATRIX!
#define WIDTH 32 // Matrix width (pixels)
#define BUTTON_UP 2
#define BUTTON_DOWN 3
#define N_COLORS 10
#define EMOJI_SIDE 16
#define CLOCK_ACTIVATION_COMMAND "[clock]"

#define match(X)  if (strcmp(unicode.c_str(), #X) == 0) { showBmp(X); }

#if 32==HEIGHT
uint8_t addrPins[] = {17, 18, 19, 20};
#else
uint8_t addrPins[] = {17, 18, 19};
#endif
uint8_t rgbPins[] = {7, 8, 9, 10, 11, 12};
uint8_t clockPin = 14;
uint8_t latchPin = 15;
uint8_t oePin = 16;
uint16_t colors[N_COLORS];

Adafruit_Protomatter matrix(WIDTH, 4, 1, rgbPins, sizeof(addrPins), addrPins, clockPin, latchPin, oePin, false);
AdafruitIO_WiFi io(IO_USERNAME, IO_KEY, WIFI_SSID, WIFI_PASS, SPIWIFI_SS, NINA_ACK, NINA_RESETN, NINA_GPIO0, &SPIWIFI);
AdafruitIO_Time *seconds = io.time(AIO_TIME_SECONDS);
Adafruit_LIS3DH accel = Adafruit_LIS3DH();

long previousMillis = 0;
long interval = 1000;
bool scrolling = false;
bool offline = false;
bool clock = false;

Bounce ubouncer = Bounce();
Bounce dbouncer = Bounce();

AdafruitIO_Feed *counter = io.feed("messagepanel");
String lastMessage;
int16_t scrollPosition = 0;

void err(int x) {
  uint8_t i;
  pinMode(LED_BUILTIN, OUTPUT); // Using onboard LED
  for (i = 1;; i++) { // Loop forever...
    digitalWrite(LED_BUILTIN, i & 1); // LED on/off blink to alert user
    delay(x);
  }
}

void drawText(const char *text, bool resetPosition = true, uint16_t color = 0xffff, uint16_t textSize = 1) {
  // Serial.printf("%s @ %d, %d\n", text, matrix.getCursorX(), matrix.getCursorY());

  //font size 1 is 6x8, 2 is 12x16, 3 is 18x24, etc
  if (textSize > 1 + matrix.height()/16) {
    textSize = 1 + matrix.height()/16;
  }

  if (resetPosition) {
    matrix.setCursor(1, 0);
  }
  if (scrolling) {
    int16_t cur = matrix.getCursorX();
    // Offscreen on the right
    if (cur > WIDTH) {
      return;
    }
    // Offscreen on the left
    if (cur < -(textSize * 6)) {
      uint16_t step = textSize * 6;
      matrix.setCursor(cur + step, 0);
      return;
    }
  } else {
    if (matrix.getCursorX() == 0 && matrix.getCursorY() == HEIGHT/2) {
      matrix.setCursor(1, HEIGHT/2);
    }
    if (matrix.getCursorX() == (WIDTH - 1) && matrix.getCursorY() == 0) {
      matrix.setCursor(1, HEIGHT/2);
    }
  }

  matrix.setTextSize(textSize);
  matrix.setTextColor(color);
  matrix.print(text);
  matrix.show();
}

// This function is called whenever a 'messagepanel' message
// is received from Adafruit IO. it was attached to
// the counter feed in the setup() function above.
void handleMessage(AdafruitIO_Data *data) {
  if (offline) {
    return;
  }
  String message = data->toString();
  clock = message.equals(CLOCK_ACTIVATION_COMMAND);
  if (clock) {
    return;
  }

  if (message[0] > 0x7f) {
    //Not valid ascii, assume emoji
    return handleEmoji(message);
  }
  lastMessage = message;
  if (!scrolling) {
    drawMessage();
  }
}

void handleSecs(char *data, uint16_t len) {
  long now = String(data).toInt();
  setTime(now);
  Serial.print("Seconds Feed: ");
  Serial.println(now);
  if (clock) {
    drawClock();
  }
}

void drawMessage() {
  String message = String(lastMessage);
  String plainText = String(message);
  message.replace("_", " ");
  uint16_t color = colors[0];
  uint16_t textSize = 1;
  uint16_t strpos = 0;
  uint16_t colorStartIndex = 0, colorEndIndex = 0;
  uint16_t sizeStartIndex = 0, sizeEndIndex = 0;
  boolean paramRead = false;

  matrix.setCursor(scrollPosition, 0);
  matrix.fillScreen(0); // Clear screen

  // Strip color info
  strpos = 0;
  while (strpos < plainText.length()) {
    colorStartIndex = plainText.indexOf('{');
    colorEndIndex = plainText.indexOf('}');
    plainText.remove(colorStartIndex, colorEndIndex - colorStartIndex + 1);
    strpos++;
  }

  // Strip size info
  strpos = 0;
  while (strpos < plainText.length()) {
    sizeStartIndex = plainText.indexOf('<');
    sizeEndIndex = plainText.indexOf('>');
    plainText.remove(sizeStartIndex, sizeEndIndex - sizeStartIndex + 1);
    strpos++;
  }
  //Serial.println(plainText);

  for (uint16_t i = 0; i < message.length(); i++) {
    if (message.charAt(i) == '{') {
      paramRead = true;
      colorStartIndex = i + 1;
    } else if (message.charAt(i) == '}') {
      paramRead = false;
      int wheelPos = atoi(message.substring(colorStartIndex, i).c_str());
      if (wheelPos < N_COLORS) {
        color = colors[wheelPos];
      } else {
        color = colors[0];
      }
      //Serial.printf("Color: %x\n", color);
    } else if (message.charAt(i) == '<') {
      paramRead = true;
      sizeStartIndex = i + 1;
    } else if (message.charAt(i) == '>') {
      paramRead = false;
      textSize = atoi(message.substring(sizeStartIndex, i).c_str());
      // Serial.printf("Text size: %d\n", textSize);
    } else {
      if (paramRead) {
        continue;
      }
      drawText(message.substring(i, i + 1).c_str(), false, color, textSize);
    }
  }
}

uint8_t emoji_shown = 0;
#define MAX_POINTS 8
void handleEmoji(String message) {
  emoji_shown = 0;
  matrix.fillScreen(0);
  matrix.show();

  Serial.print("message length ");
  Serial.println(message.length());

  utf8_decode_init((char*)message.c_str(), message.length());
  int points[MAX_POINTS] = {0};
  size_t len = 0;

  do {
    points[len++] = utf8_decode_next();
    if (points[len-1] == UTF8_ERROR) {
      Serial.printf("Error with utf8: %s\n", message);
      return;
    }

    String unicode = "_";
    for(size_t i = 0; i < len && i < MAX_POINTS; i++) {
      unicode = unicode + "_" + String(points[i], HEX);
    }
    Serial.printf("unicode %d: %s\n", len, unicode.c_str());

    uint8_t prev_shown = emoji_shown;


    match(__1f600) match(__1f601) match(__1f602) match(__1f603) match(__1f604) match(__1f605) match(__1f606) match(__1f607) match(__1f608) match(__1f609) match(__1f60a) match(__1f60b) match(__1f60c) match(__1f60d) match(__1f60e) match(__1f60f)
    match(__1f610) match(__1f611) match(__1f612) match(__1f613) match(__1f614) match(__1f615) match(__1f616) match(__1f617) match(__1f618) match(__1f619) match(__1f61a) match(__1f61b) match(__1f61c) match(__1f61d) match(__1f61e) match(__1f61f)
    match(__1f620) match(__1f621) match(__1f622) match(__1f623) match(__1f624) match(__1f625) match(__1f626) match(__1f627) match(__1f628) match(__1f629) match(__1f62a) match(__1f62b) match(__1f62c) match(__1f62d) match(__1f62e) match(__1f62f)
    match(__1f630) match(__1f631) match(__1f632) match(__1f633) match(__1f634) match(__1f635) match(__1f636) match(__1f637) match(__1f638) match(__1f639) match(__1f63a) match(__1f63b) match(__1f63c) match(__1f63d) match(__1f63e) match(__1f63f)
    match(__1f640) match(__1f641) match(__1f642) match(__1f643) match(__1f644) match(__1f645) match(__1f646) match(__1f647) match(__1f648) match(__1f649) match(__1f64a) match(__1f64b) match(__1f64c) match(__1f64d) match(__1f64e) match(__1f64f)

    match(__1f44d) match(__1f4a9) match(__1f525) match(__1f970) match(__1f97a) match(__1f9a0) match(__2615) match(__2728) match(__2764_fe0f) match(__1f389)

    // Keycaps
    match(__0023_fe0f_20e3) match(__002a_fe0f_20e3) match(__0030_fe0f_20e3) match(__0031_fe0f_20e3) match(__0032_fe0f_20e3) match(__0033_fe0f_20e3) match(__0034_fe0f_20e3) match(__0035_fe0f_20e3) match(__0036_fe0f_20e3) match(__0037_fe0f_20e3) match(__0038_fe0f_20e3) match(__0039_fe0f_20e3)

    // Food
    match(__1f32d) match(__1f32e) match(__1f32f)
    match(__1f330) match(__1f331) match(__1f332) match(__1f333) match(__1f334) match(__1f335) match(__1f336_fe0f) match(__1f337) match(__1f338) match(__1f339) match(__1f33a) match(__1f33b) match(__1f33c) match(__1f33d) match(__1f33e) match(__1f33f)
    match(__1f340) match(__1f341) match(__1f342) match(__1f343) match(__1f344) match(__1f345) match(__1f346) match(__1f347) match(__1f348) match(__1f349) match(__1f34a) match(__1f34b) match(__1f34c) match(__1f34d) match(__1f34e) match(__1f34f)
    match(__1f350) match(__1f351) match(__1f352) match(__1f353) match(__1f354) match(__1f355) match(__1f356) match(__1f357) match(__1f358) match(__1f359) match(__1f35a) match(__1f35b) match(__1f35c) match(__1f35d) match(__1f35e) match(__1f35f)
    match(__1f360) match(__1f361) match(__1f362) match(__1f363) match(__1f364) match(__1f365) match(__1f366) match(__1f367) match(__1f368) match(__1f369) match(__1f36a) match(__1f36b) match(__1f36c) match(__1f36d) match(__1f36e) match(__1f36f)
    match(__1f370) match(__1f371) match(__1f372) match(__1f373) match(__1f374) match(__1f375) match(__1f376) match(__1f377) match(__1f378) match(__1f379) match(__1f37a) match(__1f37b)

    match(__1f382)
    match(__1f44c)
    match(__1f645_200d_2642_fe0f)
    match(__1f646_200d_2642_fe0f)
    match(__1f926_200d_2642_fe0f) match(__1f926)
    match(__1f937_200d_2642_fe0f) match(__1f937)
    match(__274c)


    if (points[len-1] == UTF8_END) {
      Serial.println("UTF8_END");
      break;
    }

    if (emoji_shown != prev_shown) {
      // Clear code points for displayed emoji
      for(size_t i = 0; i < MAX_POINTS; i++) {
        points[i] = 0;
      }
      len = 0;
    }
  } while (utf8_decode_at_byte() < message.length());
}

void showBmp(const unsigned char *data) {
  return showBmp((uint8_t*)data);
}

void showBmp(uint8_t *data) {
  // Emoji are square, 16x16
  matrix.drawRGBBitmap(emoji_shown * EMOJI_SIDE, 0, (uint16_t*)data, EMOJI_SIDE, EMOJI_SIDE);
  matrix.show();

  emoji_shown++;
  if (emoji_shown >= WIDTH/EMOJI_SIDE) {
    emoji_shown = 0;
  }
}

void drawClock() {
#if 32==HEIGHT
  drawClock32();
#else
  drawClock16();
#endif
}

void drawClock32() {
  matrix.fillScreen(0); // Clear screen
  uint8_t tzHour = (hour() + 16) % 24;

  char hours[3] = {0};
  char minutes[3] = {0};

  char display[10] = {0};
  sprintf(hours, "%02d", tzHour);
  sprintf(minutes, "%02d", minute());

  drawText(hours, true, colors[2], 2);
  drawText(":", false, colors[5], 2);
  drawText(minutes, false, colors[6], 2);
}

void drawClock16() {
  matrix.fillScreen(0); // Clear screen

  char hours[3] = {0};
  char minutes[7] = {0};
  uint8_t tens = minute() / 10;
  uint8_t ones = minute() % 10;

  // +24(so the modulo will end up positive) -8 (my timezone)
  uint8_t tzHour = (hour() + 16) % 24;

  sprintf(hours, "%02d", tzHour);
  sprintf(minutes, "%d    %d", tens, ones);

  drawText(hours, true, colors[1], 2);
  drawText(minutes, false, colors[2], 1);
}

void Enable() {
  Serial.println("Enable");
  offline = false;
  matrix.resume();
  counter->get();
}

void Disable() {
  Serial.println("Disable");
  offline = true;
  matrix.setCursor(scrollPosition, 0);
  matrix.fillScreen(0); // Clear screen
  matrix.stop();
}

// Debugging
void Repeats() {
  if (offline) {
    Enable();
  } else {
    Disable();
  }
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUTTON_UP, INPUT_PULLUP);
  ubouncer.attach(BUTTON_UP);
  ubouncer.interval(3);
  pinMode(BUTTON_DOWN, INPUT_PULLUP);
  dbouncer.attach(BUTTON_DOWN);
  dbouncer.interval(3);

  Serial.begin(115200);
  // while (!Serial) delay(10);

  colors[0] = matrix.color565(64, 64, 64); // Dark Gray
  colors[1] = matrix.color565(120, 79, 23); // Brown
  colors[2] = matrix.color565(228, 3, 3); // Red
  colors[3] = matrix.color565(255, 140, 0); // Orange
  colors[4] = matrix.color565(255, 237, 0); // Yellow
  colors[5] = matrix.color565(0, 128, 38); // Green
  colors[6] = matrix.color565(0, 77, 255); // Blue
  colors[7] = matrix.color565(117, 7, 135); // Purple
  colors[8] = matrix.color565(128, 128, 128); // Olive
  colors[9] = matrix.color565(0, 0, 127); // Navy

  ProtomatterStatus status = matrix.begin();
  Serial.printf("Protomatter begin() status: %d\n", status);
  matrix.fillScreen(0);
  matrix.setRotation(2);
  matrix.setTextWrap(!scrolling);
  drawText("WIFI");

  Serial.print("Connecting...");
  io.connect();
  counter->onMessage(handleMessage);
  seconds->onMessage(handleSecs);

  while (io.mqttStatus() < AIO_CONNECTED) {
    drawText(".");
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.println(io.statusText());

  matrix.fillScreen(0);
  counter->get();

  Alarm.alarmRepeat(7, 30, 0, Enable);
  Alarm.alarmRepeat(21, 30, 0, Disable);
  // Alarm.timerRepeat(15, Repeats);            // timer for every 15 seconds    

  if (!accel.begin(0x19)) {
    Serial.println("Couldn't find accelerometer");
  }
  accel.setRange(LIS3DH_RANGE_2_G); // 2, 4, 8 or 16 G!
  accel.setClick(1, CLICKTHRESHHOLD);
}

void loop() {
  io.run();
  ubouncer.update();
  dbouncer.update();
  Alarm.delay(100); // wait one second between clock display

  if ( ubouncer.fell() ) {
    Serial.println(F("UP BUTTON."));
    interval += 200;
    Enable();
  }
  if ( dbouncer.fell() ) {
    Serial.println(F("DOWN BUTTON."));
    interval -= 200;
    Disable();
  }

  long currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
/*
    Serial.print("io.networkStatus: "); Serial.print(io.networkStatus()); Serial.print("\n");
    Serial.print("io.mqttStatus: "); Serial.print(io.mqttStatus()); Serial.print("\n");
    Serial.print("WiFi.status: "); Serial.print(WiFi.status()); Serial.print("\n");
*/

    if (offline) {
      return;
    }

    if (scrolling) {
      drawMessage();
      if (matrix.getCursorX() >= 0) {
        scrollPosition--;
      } else {
        scrollPosition = WIDTH;
      }
    }
  }
}
